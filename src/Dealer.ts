import {Player} from "./Player";

export class Dealer extends Player{
    constructor(){
        super("Dealer");
    }
}