import {BlackJack} from "./BlackJack";

class Main{

    blackJack: BlackJack;

    constructor(){
        this.blackJack = new BlackJack();
    }

    init(){
        console.log("TypeScripted BlackJack");
        this.blackJack.addPlayer("Joe");
        this.blackJack.start();

        this.blackJack.hit();
        this.blackJack.fold();
    }
}

let game:Main = new Main();
game.init();