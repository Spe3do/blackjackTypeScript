import {Player} from "./Player";
import {Deck} from "./Deck";
import {Dealer} from "./Dealer";
import {Card} from "./Card";

export class BlackJack{
    players: Player[];
    deck: Deck;
    currentPlayerIndex: number;
    currentPlayer: Player;
    gameOver: boolean;

    constructor() {
        this.gameOver = false;
        this.players = [];
        this.deck = new Deck();
        this.currentPlayerIndex = 0;
        this.players.push(new Dealer());
        this.currentPlayer = this.players[this.currentPlayerIndex];
    }

    draw():Card{
        let card = this.deck.draw();
        this.currentPlayer.drawCard(card);
        return card;
    }

    start(){
        if(this.players.length >1) {
            //Every player start with 2 cards
            for (let i = 0; i < 2; i++) {
                for (let player of this.players) {
                    this.draw();
                    this.turn();
                }
            }

            //Print initial state:
            console.log("===DRAW===");
            this.printAllHands();
            console.log("===END OF DRAW===\n");

            //Switch to the second player due to the first is the dealer
            this.currentPlayerIndex = 1;
            this.currentPlayer=this.players[this.currentPlayerIndex];
            this.evaluate();
        }
    }

    hit(){
        if(!this.gameOver){
            console.log("\n==========================");
            console.log(this.currentPlayer.name + ": HIT")
            console.log("New card: ")
            console.log(this.draw().toString());
            this.printCurrentPlayerHand();
            this.evaluate();
        }
    }

    stay(){
        if(!this.gameOver) {
            console.log("\n==========================");
            console.log(this.currentPlayer.name + ": STAY")
            this.turn();
            this.printCurrentPlayerHand();
            this.evaluate();
        }
    }

    fold(){
        if(!this.gameOver) {
            console.log("\n==========================");
            console.log(this.currentPlayer.name + ": FOLD");
            this.players[this.currentPlayerIndex].hand = [];
            this.printCurrentPlayerHand();
            this.evaluate();
        }
    }

    turn(){
        this.currentPlayerIndex++;
        if(this.currentPlayerIndex === this.players.length){
            this.currentPlayerIndex = 0;
        }
        this.currentPlayer = this.players[this.currentPlayerIndex];
    }

    addPlayer(name:string){
        this.players.push(new Player(name));
    }

    evaluate():Player{
        let winners: Player[] = [];

        //check if any of the players has exactly 21
        for(let player of this.players){
            if (player.getHandValue() === 21){
                winners.push(player);
            }
        }
        if(winners.length === 1){
            console.log("The winner is: "+winners[0].name);
            this.gameOver = true;
            return winners[0];
        }

        if (winners.length > 1){
            this.gameOver = true;
            console.log("It's TIE")
        };



    }

    printCurrentPlayerHand(){
        console.log("\n");
        console.log(this.currentPlayer.toString());
        console.log(this.currentPlayer.name + " hand's value " + this.currentPlayer.getHandValue());
    }

    printAllHands() {
        for(let player of this.players){
            console.log("\n");
            console.log(player.toString());
            console.log(player.name + " hand's value " + player.getHandValue());
        }
    }

}
