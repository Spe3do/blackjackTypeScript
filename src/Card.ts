export class Card{
    name: string;
    valueString: string;
    value: number;

    constructor(name:string, value:string){
        this.name = name;
        this.valueString = value;
        this.value = this.convertValue(value);
    }

    convertValue(value: string): number {
        switch (value){
            case 'A':
                return 11;
            case 'K':
            case 'Q':
            case 'J':
                return 10;
            default:
                return parseInt(value);
        }
    }

    getValue(): number {
        return this.value;
    }

    toString(): string {
        return "Name: \t" + this.name + " - " + this.valueString + "\tValue: "+this.value;
    }
}