import {Card} from "./Card";

export class Player {
    private _name: string;
    private _hand: Card[];

    constructor(name: string){
        this._name=name;
        this._hand = [];
    }


    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get hand(): Card[] {
        return this._hand;
    }

    set hand(value: Card[]) {
        this._hand = value;
    }

    drawCard(card: Card){
        this._hand.push(card);
    }

    getHandValue(): number{
        let sum:number = 0;
        for(let card of this._hand){
            sum += card.getValue();
        }
        return sum;
    }

    toString():string {
        let result:string = "";
        result += ("Cards in "+this.name+"'s hand:");

        for(let card of this._hand){
            result+= "\n";
            result +=card.toString();
        }

        return result;

    }
}
