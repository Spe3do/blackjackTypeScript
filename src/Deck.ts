import {Card} from "./Card";

export class Deck {
    static SUITS = ['HEART', 'DIAMOND', 'CLUB', 'SPADE'];
    static RANKS = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K'];

    cards: Card[];
    currentCardIndex: number;

    constructor() {
        this.cards = [];
        this.currentCardIndex = 0;
        for (var i = 0; i < 4; i++) {
            for (var s = 0; s < 13; s++) {
                this.cards.push(new Card(Deck.SUITS[i], Deck.RANKS[s]));
            }
        }
        this.shuffle(5);
    }

    shuffle(times?:number) {
        for (var i = 0; i < (times||1); i++) {
            this.cards.sort(function() { return (0.5 - Math.random()); });
        }
    }

    draw(): Card{
        return this.cards[this.currentCardIndex++];
    }
}